iTunes Sharing Connection
===========

共有している iTunes Library への接続状況を Growl通知 するスクリプト

利用環境
------------

利用するためには、`growlnotify`のインストールが必要です。
	
	growlnotifyによるCLIからのGrowl通知 - 祈れ、そして働け ～ Ora et labora
	( http://d.hatena.ne.jp/tetsuyai/20110917/1316234149 )
		
利用方法
------------
	
	$ git clone https://bitbucket.org/dentyu/growl-itunes-sharing-connection.git ~/Desktop/growl-itunes-sharing-connection
	
	$ crontab -l | { cat; echo "*/10 * * * * cd ~/Desktop/growl-itunes-sharing-connection/ && ruby ./run.rb >/dev/null 2>&1"; } | crontab 


