# -*- coding: utf-8 -*-

require 'open3'
require './lib/notify.rb'

command = 'lsof -i tcp:daap'
stdin, stderr, status = Open3.capture3 command

if !status.success?
  p "Error: #{stderr}" 
  return false
end

result = stdin.split("\n")
result.shift

connections = result.map{|x| x.scan(/TCP\s(.*):daap/); $1 }
connections.delete '*'

if connections.size > 0
  Notify.new(
    title: "iTunes Sharing : #{connections.count} users", 
    message: connections.join("\n")
  ).to_growl
end
