# -*- coding: utf-8 -*-

class Notify
  attr_accessor :title, :message, :icon

  GROWLNOTIFY_PATH = '/usr/local/bin/growlnotify'

  def to_growl
    return false if !File.exist? GROWLNOTIFY_PATH

    command  = GROWLNOTIFY_PATH
    command += " -t '#{title}'" if !title.empty?
    command += " -m '#{message}'" if !message.empty?
    command += icon.nil? ? " -a /Applications/iTunes.app" : " -I '#{icon}'"
    system command
  end

  def to_stdin
    p "[#{title}] #{message}"
  end

  # Initialize
  def initialize(*args)
    options   = args.first

    @title   = options[:title]
    @message = options[:message]
    @icon    = options[:icon]
  end

end
